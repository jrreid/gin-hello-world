package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestRootRoute(t *testing.T) {
	router := gin.Default()
	router.GET("/", RootRouteHandler)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "{\"message\":\"Hello World!\"}", w.Body.String())
}

func TestReadFileRoute(t *testing.T) {
	router := gin.Default()
	router.GET("/readfile", ReadFileHandler)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/readfile?filename=data.txt", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "{\"contents\":\"this is data\\n\"}", w.Body.String())

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/readfile", nil)
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusBadRequest, w.Code)
	assert.Equal(t, "{\"error\":\"filename not specified\"}", w.Body.String())
}
