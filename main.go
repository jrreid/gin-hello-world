package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

// Default port to listen on
var ListenPort = 5000

func RootRouteHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "Hello World!",
	})
}

// Intentionally insecure function which allows for arbitrary path traversal
func ReadFileHandler(c *gin.Context) {
	data, err := os.ReadFile(c.Query("filename"))
	if c.Query("filename") == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "filename not specified"})
		return
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"contents": string(data)})
}

func LoadLocalVars() {
	godotenv.Load(".env")

	if os.Getenv("LISTEN_PORT") == "" {
		fmt.Println("No port specified, listening on " + strconv.Itoa(ListenPort))
		return
	} else {
		ListenPort, _ = strconv.Atoi(os.Getenv("LISTEN_PORT"))
	}
}

func main() {

	LoadLocalVars()

	router := gin.Default()

	router.GET("/", RootRouteHandler)
	router.GET("/readfile", ReadFileHandler)

	router.Run(":" + strconv.Itoa(ListenPort))
}
